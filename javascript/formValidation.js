/*
 *  Author:        Philip Gedgaud
 *	Date Created:  4/26/2011
 *	Last Modified: 4/26/2011
 *  Purpose:       Functions for valdiating fields of forms
 */

function validate(field)
{	
	//Check to see if form field is empty then display error if it is
	var errorMessage = "Please enter a value!";
	var errorID = field.id + "Error";
	var fieldID= field.id;
	//document.getElementById(errorID).innerHTML = "";
	
	document.getElementById(errorID).innerHTML = "";
	if( field.value == "")
	{
		document.getElementById(errorID).innerHTML = errorMessage;
	}
}

function validateEmail()
{   
	var errorMessage = "Please enter an email address!";
	var errorID = "validateemail";
	document.getElementById(errorID).innerHTML = "";
	//Check to see if email is empty then display message
	if( document.getElementById("email").value == "")
	{
		document.getElementById(errorID).innerHTML = errorMessage;
		return;
	}
					
	//Check that the email is in valid format
	var atpos = document.getElementById("email").value.indexOf("@");
	var dotpos = document.getElementById("email").value.lastIndexOf(".");
	if (atpos < 1 || dotpos < atpos+2 || dotpos+2 >= document.getElementById("email").value.length)
	{
		document.getElementById("validateemail").innerHTML = "Not a valid email please use form name@email.com";
	}
					
}

function validatePhoneNumber()
{	
	var errorMessage = "Please enter an phone number!";
	var errorID = "validatephone";
	document.getElementById(errorID).innerHTML = "";
	
	//Check to see if email is empty then display message
	if( document.getElementById("phone").value == "")
	{
		document.getElementById(errorID).innerHTML = errorMessage;
		return;
	}
				
	var phoneValidator= /^\(\d\d\d\)-\d\d\d-\d\d\d\d$/; 
	var phoneNumber = document.getElementById("phone").value;
	if ( !phoneNumber.match(phoneValidator) ) 
	{ 
		document.getElementById(errorID).innerHTML = "Please enter phone number in 123-456-7890 format!!"; 
	}
}


// Pg <:3  )~~~ 