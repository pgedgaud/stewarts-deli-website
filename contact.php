<?php print('<?xml version="1.0" encoding="utf-8" ?>'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<!--
	Title:         contact.php
	Author:        Philip Gedgaud
	Date Created:  2/13/2011
	Last Modified: 5/5/2011
	Purpose:       Gives a some contact information as well as provides a contact form that emails the owner.
-->
 
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template.dwt" codeOutsideHTMLIsLocked="false" -->
	<head>
		<title>Stewart's Deli</title>
		<meta name="description" content="A deli in Fargo serving up delicous  wholesome sandwiches, salads, and soup." />
		<meta name="author" content="Philip Gedgaud" />
		<meta name="keywords" content="deli, food, sandwiches, soup, salad" />
		
		<!-- favicon 16x16 -->
		<link rel="shortcut icon" href="images/favicon.ico" />

		<!-- CSS stylesheet -->
		<link rel="stylesheet" type="text/css" href="css/screen.css" />
	
		<!-- Print stylesheet -->
		<link rel="stylesheet" type="text/css" media="print" href="css/print.css" />
	
		<!-- JavaScript -->
        <script type="text/javascript" src="javascript/formValidation.js"></script>
		<!-- InstanceBeginEditable name="headRegion" --><!-- InstanceEndEditable -->
	</head>
	
	<body>
 		
 		<div id="header" class="group">
	    <h1>Stewart's Deli</h1>
			
			<div id="nav" class="group">
	  	  		<ul>
					<li><a href="index.html" title="Go to the Home Page"><span class="linkContent">Home <span class="navSub">Main Page</span></span></a></li>
					<li><a href="menu.html" title="Go to the Menu"><span class="linkContent">Menu <span class="navSub">Delicious Food</span></span></a></li>
					<li><a href="about.html" title="Go to the About Us Page"><span class="linkContent">About Us <span class="navSub">Company Info</span></span></a></li>
					<li><a href="contact.php" id="lastLink" title="Go to the Contact Page"><span class="linkContent">Contact Us <span class="navSub">Get In Touch</span></span></a></li>
            	</ul>
			</div>
		</div><!-- End of header -->
		
		<div id="main" class="group"><!-- InstanceBeginEditable name="mainContent" -->
        


        <div id="sidebar" class="group">
          <p><a href="survey.php" title="Take the survey!!">Take this <br />
            survey and <br />
            get free<br />
            food.</a></p>
        </div>
        
        <h2>Drop us a line!</h2>
        
        <p>Our hours are:<br />
          Monday - Saturday<br />
          6am - 4pm
          <br />
        </p>
        <p>You can contact us as follows.<br />
        
        Mail:</p>
        <address> 4955 17th Avenue South<br /> Fargo, ND 58103</address><br />
        <p>Phone: <br />
        Email: <a href=""></a><br />
        Facebook: <a href=""></a></p>
		<?php 
			//get input
			extract( $_POST );
			$iserror = false;
			
			if ( isset ( $submit ))
         	{
				//Test to make sure everything is filled in
				if ( $name == "" ) 
            	{
               		$formerrors[ "nameError" ] = true;
               		$iserror = true;
            	}
			
				if ( $email == "" ) 
            	{
               		$formerrors[ "emailError" ] = true;
               		$iserror = true;
            	}
				
				//Test that the email is a valid email
				if ( !eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$" , $email) )
				{
					$formerrors[ "emailError" ] = true;
               		$iserror = true;	
				}
				
				if ( $phone == "" )
				{
					$formerrors[ "phoneError" ] = true;
               		$iserror = true;		
				}
				
				//Tests that the phone number is a valid phone number
				if ( !ereg( "^\([0-9]{3}\)-[0-9]{3}-[0-9]{4}$", $phone ) )
				{
					$formerrors[ "phoneError" ] = true;
               		$iserror = true;		
				}
				
				if ( $message == "" )
				{
					$formerrors[ "messageError" ] = true;
               		$iserror = true;		
				}
				
				if(!$iserror)
				{	
					//Mail the results
				 	$to = ""; // Change this to where every you want the results sent
 					$subject = "Contact Request";
					$body = "Here is what the customer stated in there contact form\n
							 Name: $name\n
							 Email: $email\n
							 Phone: $phone\n
							 Message: $message\n";
					
					//Can be added to have a from field in the email
					//$email_from = "webmaster@m4-hava.org";
					//ini_set("sendmail_from", $email_from);
					//$headers = "From: $email_from";
					
	 				if ( mail($to, $subject, $body) )  //add if using from feilds above $headers
					{
   						echo("<p>Message successfully sent!</p>");
  					} 
					else 
					{
   						echo("<p>Message delivery failed... Please try again.</p>");
  					}
				}//End of no error
			}//End of if submit
		?>
		<p>Or you can use the contact form below.</p>
		<form id="form1" method="post" action="">
        	<fieldset>
 			<label for="name">Name:</label><br />
  			<input type="text" name="name" id="name" onblur="validate(this)" value="<?php print("$name"); ?>"/>
  			<span id="nameError" class="required">*
  			<?php
				if( $formerrors['nameError'] )
				{
					print('<p>Please enter your name.</p>');
				}
			?>
  			</span><br />
  			
            <label for="email">Email:</label><br />
  			<input type="text" name="email" id="email" onblur="validateEmail()" value="<?php print("$email"); ?>" />
            <span id="validateemail" class='required'>*
  			<?php
				if( $formerrors['emailError'] )
				{
					print('<p>Please enter your email in the form name@email.com.</p>');
				}
			?>
  			</span><br />
  			
            <label for="phone">Phone:</label><br />
  			<input type="text" name="phone" id="phone" onblur="validatePhoneNumber()" value="<?php print("$phone"); ?>" />
  			<span id="validatephone" class='required'>*
    		<?php
				if( $formerrors['phoneError'] )
				{
					print('<p>Please enter your phone number in the form (123)-456-7890</p>');
				}
			?>
  			</span><br />
  			
            <label for="message">Message:</label><br />
  			<textarea name="message" id="message" cols="45" rows="5" onblur="validate(this)"><?php print("$message"); ?></textarea>
            <span id="messageError" class='required'>*
            <?php
				if( $formerrors['messageError'] )
				{
					print('<p>Please leave a message.</p>');
				}
			?>
            </span><br />
  			
            <input type="submit" name="submit" id="submit" value="Submit" />
  			<input type="reset" name="reset" id="reset" value="Reset" />
  		</fieldset>
	</form>
	
    <p>Get directions using this Google map!!</p>
    <img src="images/outside.jpg" alt="Oustide picture of the deli" width="300" height="425" class="imgRight" />
<div id="map">
        <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=stewarts+deli&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=44.069599,105.556641&amp;ie=UTF8&amp;hq=stewarts+deli&amp;hnear=&amp;ll=46.854909,-96.869652&amp;spn=0.071946,0.071946&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=stewarts+deli&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=44.069599,105.556641&amp;ie=UTF8&amp;hq=stewarts+deli&amp;hnear=&amp;ll=46.854909,-96.869652&amp;spn=0.071946,0.071946" style="color:#0000FF;text-align:left">View Larger Map</a></small>
        </div>
<!-- InstanceEndEditable --></div>
		<!-- End of Main -->	
<div id="footer" class="group">
			<div id="footNav">
				<ul>
					<li><a href="index.html" title="Go to Home Page">Home</a></li>
					<li><a href="menu.html" title="Go to the Menu">Menu</a></li>
					<li><a href="about.html" title="Go to the About Us Page">About Us</a></li>
					<li><a href="contact.php" title="Go to the Contact Page">Contact Us</a></li>
				</ul>
			</div>
			<div id="socialMedia">
				<p><a href="" title="Visit our Facebook Page" ><img src="images/fb_logo.jpg" alt="Facebook Logo"/> Check us out on Facebook!</a></p>
				<p><a href="" title="Visit our Twitter Page"><img src="images/twitterLogo.png" alt="Twitter Logo"/> Check us on Twitter!</a></p>
			</div>
			<div id="copyright">
			  <p>Please don't steal our stuff. Ask us before you use anything from our site.</p>
			  <p>Pictures taken by nikkijeanphotography.com.</p>
			</div>
		</div><!-- End of footer -->
	
	</body>
<!-- InstanceEnd --></html>

<!-- Pg    <:3  )~~~   -->