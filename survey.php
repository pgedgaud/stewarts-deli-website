<?php print('<?xml version="1.0" encoding="utf-8" ?>') ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<!--
	Title:         survey.php
	Author:        Philip Gedgaud
	Date Created:  2/13/2011
	Last Modified: 5/5/2011
	Purpose:       Allows the user to fill out a survey. When it is complete the user get a coupon. The page records the ip address so a user can get mulitple coupons one after the other.
-->
 
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template.dwt" codeOutsideHTMLIsLocked="false" -->
	<head>
		<title>Stewart's Deli</title>
		<meta name="description" content="A deli in Fargo serving up delicous  wholesome sandwiches, salads, and soup." />
		<meta name="author" content="Philip Gedgaud" />
		<meta name="keywords" content="deli, food, sandwiches, soup, salad" />
		
		<!-- favicon 16x16 -->
		<link rel="shortcut icon" href="images/favicon.ico" />

		<!-- CSS stylesheet -->
		<link rel="stylesheet" type="text/css" href="css/screen.css" />
	
		<!-- Print stylesheet -->
		<link rel="stylesheet" type="text/css" media="print" href="css/print.css" />
	
		<!-- JavaScript -->
        <script type="text/javascript" src="javascript/formValidation.js"></script>
		<!-- InstanceBeginEditable name="headRegion" --><!-- InstanceEndEditable -->
	</head>
	
	<body>
 		
 		<div id="header" class="group">
	    <h1>Stewart's Deli</h1>
			
			<div id="nav" class="group">
	  	  		<ul>
					<li><a href="index.html" title="Go to the Home Page"><span class="linkContent">Home <span class="navSub">Main Page</span></span></a></li>
					<li><a href="menu.html" title="Go to the Menu"><span class="linkContent">Menu <span class="navSub">Delicious Food</span></span></a></li>
					<li><a href="about.html" title="Go to the About Us Page"><span class="linkContent">About Us <span class="navSub">Company Info</span></span></a></li>
					<li><a href="contact.php" id="lastLink" title="Go to the Contact Page"><span class="linkContent">Contact Us <span class="navSub">Get In Touch</span></span></a></li>
            	</ul>
			</div>
		</div><!-- End of header -->
		
		<div id="main" class="group"><!-- InstanceBeginEditable name="mainContent" -->
        	<?php 
				//get input
				extract( $_POST );
				$iserror = false;
			
				if ( isset ( $submit ))
         		{
					//Test to make sure everything is filled in
					if ( $eatDate == "" ) 
            		{
               			$formerrors[ "eatDateError" ] = true;
               			$iserror = true;
            		} 
					
					if ( $timeWait == "" ) 
            		{
               			$formerrors[ "timeWaitError" ] = true;
               			$iserror = true;
            		}
			
					if (!isset($taste))
					{
						$formerrors[ "tasteError" ] = true;
						$iserror = true;
					}
			
					if (!isset($selection))
					{
						$formerrors[ "selectionError" ] = true;
						$iserror = true;
					}
				
					if (!isset($quality))
					{
						$formerrors[ "qualityError" ] = true;
						$iserror = true;
					}// end if
				
					if (!isset($service))
					{
						$formerrors[ "serviceError" ] = true;
						$iserror = true;
					}// end if
					
					if (!isset($value))
					{
						$formerrors[ "valueError" ] = true;
						$iserror = true;
					}// end if
					
					if (!isset($overall))
					{
						$formerrors[ "overallError" ] = true;
						$iserror = true;
					}// end if
					
					if (!isset($moreOf))
					{
						$formerrors[ "moreOfError" ] = true;
						$iserror = true;
					}// end if
					
					if (!isset($toReturn))
					{
						$formerrors[ "toReturnError" ] = true;
						$iserror = true;
					}// end if
					
					$IPAddress = $_SERVER['REMOTE_ADDR']; //Gets the users IP address
					$numOfAddresses = 0;  //Counter for the number of IP addresses in the file
					
					//If the users ip address is in the file display warning
					$filename = "ipaddresses.txt";
					$fp = fopen($filename,'r') or die("can't open file");
					
					while( $line = fgets($fp) ) 
					{ 
	
						if( $IPAddress == (trim($line)) )
						{
							print("<p>You have already filled out a survey. <a href='index.html' title='Go to the Home Page'>Click here to go back to the home page.</a></p>");
							die();
						}
						$numOfAddresses++;
					} 
					fclose($fp);
					
					//if there are more than 1000 ip addresses in the file wipe the file.
					if( $numOfAddresses > 1000 )
					{	
						$fp = fopen($filename,'w') or die("can't open file");
						fclose($fp);
					}
					
					//If there are no errors display the coupon
					if(!$iserror)
					{	
						//Mail the results
						$to = ""; //
						$subject = "Survey Results";
						$body = "Here is what the customer stated in there survey\n The date they ate there: $eatDate.\n The time it took to get there food: $timeWait\n Current offering is what Expected: $selection\n Taste of the meal was : $taste\n The quality of the Food: $quality\n The service was: $service\n The value for your money was: $value\n The overall experiences is: $overall\n The want more of: $moreOf\n To ensure a return: $toReturn\n There email: $email\n Additional Comments: $comments\n";

						if ( mail($to, $subject, $body) ) 
						{
							echo("<p>Message successfully sent!</p>");
						} 
						else 
						{
							echo("<p>Message delivery failed...</p>");
						}
						//Append ip address to file
						$fp = fopen($filename,'a') or die("can't open file");
						$stringData = $IPAddress;
						fwrite($fp, $IPAddress);
						fwrite($fp, "\n");
						fclose($fp);
	
						print('<p>Print this page and bring it into Stewarts Deli</p>');
						print('<div id="coupon"><h3>Recieve a free dessert or side with a $5 purchase </h3>');
						$date = mktime(0,0,0,date("m"),date("d")+30,date("Y"));
						print("<p> Not good with any other offer. Expires on ".date("Y/m/d", $date).".</p>"); 
						print('</div>');
						die();
					} //end of not error
				}// end of is sumbit	
			
			//If there are any errors redisplay the form with error messages
			?>
			
			<h2>Survey</h2>
        		<p><span class='required'>*</span> indicates required fields</p>
				<form id='surveyForm' method='post' action='survey.php'>
				<table>
            		<tr>
              			<td id="labelCol"><label for='eatDate'>When did you eat at the deli?<span class='required'>*</span></label></td>
              			<td colspan='5'>
							<input type='text' name='eatDate' id='eatDate' onblur='validate(this)' value='<?php print("$eatDate"); ?>' />
							<?php
								if( $formerrors['eatDateError'] )
								{
									print('<p id="eatDateError" class="required">Please enter the date you last ate at Stewarts Deli.</p>');
								}
							?>
				 		</td>
            		</tr>
            		<tr>
              			<td><label for='timeWait'>How long did you have to wait for your food?<span class='required'>*</span></label></td>
              			<td colspan='5'>
              				<input type='text' name='timeWait' id='timeWait' onblur='validate(this)' value='<?php print("$timeWait"); ?>'/>
							<?php
								if( $formerrors['timeWaitError'] )
								{
			 						print('<p id="timeWaitError" class="required">Please enter the amount of time it took to get your food.</p>');	
								}
							?>            
            			</td>
            		</tr>
					<tr>
                        <td>
                            <label for='selection'>Has the current offerings been what they expected?<span class='required'>*</span></label>
                            <?php
								//Display an error message if there is no selection
                                if( $formerrors['selectionError'] )
                                {
                                    print('<p id="selectionError" class="required">Please select an answer.</p>');	
                                } 
                            ?>
                        </td>
                        <td><input type='radio' name='selection' id='selection1' value='Yes' />Yes</td>
                        <td><input type='radio' name='selection' id='selection2' value='No' />No</td>
            		</tr>
            		<tr> 
              			<td></td>
                      	<td class='radioButCol'>Excellent</td>
                      	<td class='radioButCol'>Good </td>
                      	<td class='radioButCol'>OK</td>
                      	<td class='radioButCol'>Poor</td>
                      	<td class='radioButCol'>Horrible</td>
                    </tr>
            		<tr>
						<td><label for='taste'>How was the taste of your meal<span class='required'>*</span></label>
						<?php
							//Display an error message if there is no selection
							if( $formerrors['tasteError'] )
							{
			 					print('<p id="tasteError" class="required">Please select an answer.</p>');	
							}
						?>
			 			</td>
                     	<td><input type='radio' name='taste' id='taste1' value='excellent' /></td>
                      	<td><input type='radio' name='taste' id='taste2' value='good' /></td>
                      	<td><input type='radio' name='taste' id='taste3' value='ok' /></td>
                      	<td><input type='radio' name='taste' id='taste4' value='poor' /></td>
                      	<td><input type='radio' name='taste' id='taste5' value='horrible' /></td>
                    </tr>
            		<tr>
              			<td><label for='quality'>Quality of the food<span class='required'>*</span></label>
                        <?php
							//Display an error message if there is no selection
							if( $formerrors['qualityError'] )
							{
								print('<p id="qualityError" class="required">Please select an answer.</p>');	
							}
						?>
						</td>
              			<td><input type='radio' name='quality' id='quality1' value='excellent' /></td>
              			<td><input type='radio' name='quality' id='quality2' value='good' /></td>
              			<td><input type='radio' name='quality' id='quality3' value='ok' /></td>
              			<td><input type='radio' name='quality' id='quality4' value='poor' /></td>
              			<td><input type='radio' name='quality' id='quality5' value='horrible' /></td>
            	</tr>
            	<tr>
					<td><label for='service'>How was your service?<span class='required'>*</span></label>
			  		<?php
						//Display an error message if there is no selection
						if( $formerrors['serviceError'] )
						{
							print('<p id="serviceError" class="required">Please select an answer.</p>');	
						}
					?>
              
              </td>
              <td><input type='radio' name='service' id='service1' value='excellent' /></td>
              <td><input type='radio' name='service' id='service2' value='good' /></td>
              <td><input type='radio' name='service' id='service3' value='ok' /></td>
              <td><input type='radio' name='service' id='service4' value='poor' /></td>
              <td><input type='radio' name='service' id='service5' value='horrible' /></td>
            </tr>
            <tr>
              <td><label for='value'>Please rate your visit based on value for you money<span class='required'>*</span></label>              
              		<?php
						//Display an error message if there is no selection
						if( $formerrors['valueError'] )
						{
							print('<p id="valueError" class="required">Please select an answer.</p>');	
						}
					?>
              </td>
              <td><input type='radio' name='value' id='value1' value='excellent' /></td>
              <td><input type='radio' name='value' id='value2' value='good' /></td>
              <td><input type='radio' name='value' id='value3' value='ok' /></td>
              <td><input type='radio' name='value' id='value4' value='poor' /></td>
              <td><input type='radio' name='value' id='value5' value='horrible' /></td>
            </tr>
            <tr>
              <td><label for='overall'>Overall Experience<span class='required'>*</span></label>
              		<?php
						//Display an error message if there is no selection
						if( $formerrors['overallError'] )
						{
							print('<p id="overallError" class="required">Please select an answer.</p>');	
						}
					?>
              
              </td>
              <td><input type='radio' name='overall' id='overall1' value='excellent' /></td>
              <td><input type='radio' name='overall' id='overall2' value='good' /></td>
              <td><input type='radio' name='overall' id='overall3' value='ok' /></td>
              <td><input type='radio' name='overall' id='overall4' value='poor' /></td>
              <td><input type='radio' name='overall' id='overall5' value='horrible' />
              </td>
            </tr>

			<!-- I would like to be able to contact the unhappy ones-->

			<tr>
              <td><label for='moreOf'>What do you want more of? <span class='required'>*</span></label>
              		<?php
						//Display an error message if there is no selection
						if( $formerrors['moreOfError'] )
						{
							print('<p id="moreOfError" class="required">Please select an answer.</p>');	
						}
					?>
              </td>
              <td colspan='5'>
			  <textarea name='moreOf' id='moreOf' cols='45' rows='5'><?php print("$moreOf"); ?></textarea></td>
            </tr>
            <tr>
              	<td><label for='toReturn'>What can we do better to ensure your return? <span class='required'>*</span></label>
              		<?php
						//Display an error message if there is no selection
						if( $formerrors['toReturnError'] )
						{
							print('<p id="toReturnError" class="required">Please select an answer.</p>');	
						}
					?>
              	</td>
              	<td colspan='5'>
			  <textarea name='toReturn' id='toReturn' cols='45' rows='5'><?php print("$toReturn"); ?></textarea></td>
            </tr>
            <tr>
              			<td><label for='email'>If you would like us to contact you to help resolve any problems please give us your email address and leave a comment below.</label></td>
              			<td colspan='5'>
							<input type='text' name='email' id='email' value='<?php print("$email"); ?>' size='45'/>
			
				 		</td>
            		</tr>
            <tr>
              <td><label for='comments'>Additonal Comments</label></td>
              <td colspan='5'>
			  <textarea name='comments' id='comments' cols='45' rows='5'><?php print("$comments"); ?></textarea></td>
            </tr>
          </table>
          <p>
            <label>
              <input type='submit' name='submit' id='submit' value='Submit' />
            </label>
            <input type='reset' name='reset' id='reset' value='Reset' />
          </p>
        </form>

<!-- InstanceEndEditable --></div>
		<!-- End of Main -->	
<div id="footer" class="group">
			<div id="footNav">
				<ul>
					<li><a href="index.html" title="Go to Home Page">Home</a></li>
					<li><a href="menu.html" title="Go to the Menu">Menu</a></li>
					<li><a href="about.html" title="Go to the About Us Page">About Us</a></li>
					<li><a href="contact.php" title="Go to the Contact Page">Contact Us</a></li>
				</ul>
			</div>
			<div id="socialMedia">
				<p><a href="" title="Visit our Facebook Page" ><img src="images/fb_logo.jpg" alt="Facebook Logo"/> Check us out on Facebook!</a></p>
				<p><a href="" title="Visit our Twitter Page"><img src="images/twitterLogo.png" alt="Twitter Logo"/> Check us on Twitter!</a></p>
			</div>
			<div id="copyright">
			  <p>Please don't steal our stuff. Ask us before you use anything from our site.</p>
			  <p>Pictures taken by nikkijeanphotography.com.</p>
			</div>
		</div><!-- End of footer -->
	
	</body>
<!-- InstanceEnd --></html>

<!-- Pg    <:3  )~~~   -->